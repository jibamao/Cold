﻿using OpenHardwareMonitor.Hardware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Cold
{
    /// <summary>
    /// MonitorWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MonitorWindow : Window
    {


        UpdateVisitor updateVisitor;
        Computer computer;

        public MonitorWindow()
        {
            InitializeComponent();
            this.muc_1.NameStr = "CPU";
            this.muc_1.nameStr_1 = "温度";
            this.muc_1.nameStr_2 = "风扇";
            this.muc_2.NameStr = "GPU";
            this.muc_2.nameStr_1 = "温度";
            this.muc_2.nameStr_2 = "风扇";
            this.muc_3.NameStr = "RAM";
            this.muc_3.nameStr_1 = "使用率";
            this.muc_3.nameStr_2 = "XXX";



            updateVisitor = new UpdateVisitor();
            computer = new Computer();

            computer.CPUEnabled = true;
            computer.GPUEnabled = true;
            computer.RAMEnabled = true;

            computer.Open();
            computer.Accept(updateVisitor);



            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(OnTimedEvent);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();

        }


        public void OnTimedEvent(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < computer.Hardware.Length; i++)
            {

                if (computer.Hardware[i].HardwareType == HardwareType.CPU)
                {
                    for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                    {
                        if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Temperature)
                        {
                            float CPUTem = (float)computer.Hardware[i].Sensors[j].Value;
                            this.muc_1.value_1 = CPUTem;
                            this.muc_1.valueStr_1 = CPUTem.ToString() + "℃";
                        }
                    }
                }


                if (computer.Hardware[i].HardwareType == HardwareType.GpuNvidia)
                {
                    for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                    {
                        if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Temperature)
                        {

                            float GPUTem = (float)computer.Hardware[i].Sensors[j].Value;
                            this.muc_2.value_1 = GPUTem;
                            this.muc_2.valueStr_1 = GPUTem.ToString() + "℃";
                        }
                    }
                }


                if (computer.Hardware[i].HardwareType == HardwareType.GpuAti)
                {
                    for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                    {
                        if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Temperature)
                        {

                            float GPUTem = (float)computer.Hardware[i].Sensors[j].Value;
                            this.muc_2.value_1 = GPUTem;
                            this.muc_2.valueStr_1 = GPUTem.ToString() + "℃";
                        }
                    }
                }


                if (computer.Hardware[i].HardwareType == HardwareType.RAM)
                {
                    for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                    {
                        if (computer.Hardware[i].Sensors[j].SensorType == SensorType.Load)
                        {

                            double RAMsyl = (double)computer.Hardware[i].Sensors[j].Value;
                            this.muc_3.value_1 = RAMsyl;
                            this.muc_3.valueStr_1 = Math.Round(RAMsyl, 2).ToString() + "%";

                        }
                    }
                }
            }

            }
            catch { }

        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch { }
        }
    }
}
